**Memetic evolution of training sets with adaptive radial basis kernels for support vector machines**

Jakub Nalepa, Wojciech Dudzik, and Michal Kawulok

(Paper submitted to IEEE ICPR 2020)

This repository contains: 

* The 2D (synthetically generated) datasets (binary classification)
* Our multi-fold splits of the UCI benchmark datasets used for experiments